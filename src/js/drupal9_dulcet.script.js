import 'popper.js';
import 'bootstrap';
import '@fortawesome/fontawesome-free/js/all';
import './new_additions.js';

(function () {

  'use strict';

  Drupal.behaviors.helloWorld = {
    attach: function (context) {
      console.log('Hello World');
    }
  };

})(jQuery, Drupal);
